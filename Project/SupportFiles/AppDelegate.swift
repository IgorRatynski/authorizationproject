//
//  AppDelegate.swift
//  Project
//
//  Created by Igor Ratynski on 17/01/2019.
//  Copyright © 2019 Igor Ratynski. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    typealias OptionDict = [UIApplication.LaunchOptionsKey: Any]
    var window: UIWindow?
    var initialViewController: UIViewController?
    var initialNavigationController: UIViewController?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: OptionDict?) -> Bool {

        setupRootScreen()

        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // save data
    }

}

fileprivate extension AppDelegate {

    func setupRootScreen() {
        initialNavigationController = UINavigationController(rootViewController: Factory.constructScreen(type: .login)!)
//        initialViewController = LoginViewController()

        let frame = UIScreen.main.bounds
        window = UIWindow(frame: frame)

        window!.rootViewController = initialNavigationController
        window!.makeKeyAndVisible()
    }
}
