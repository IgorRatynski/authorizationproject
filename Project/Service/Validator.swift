//
//  ValidationService.swift
//  Project
//
//  Created by Igor Ratynski on 03/02/2019.
//  Copyright © 2019 Igor Ratynski. All rights reserved.
//

import Foundation

extension String {

    typealias RegEx = Factory.Construct.RegEx

    enum ValidationType {
        case login
        case email
        case password
    }

    func isValid(_ type: Constants.RegEx) -> Bool {
        let regex = Factory.construct(element: RegEx.default(type.rawValue))
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }

}
