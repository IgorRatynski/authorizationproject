//
//  ErrorManager.swift
//  Project
//
//  Created by Igor Ratynski on 21/01/2019.
//  Copyright © 2019 Igor Ratynski. All rights reserved.
//

import Foundation

protocol ErrorManagerProtocol {
    func checkErrorType(element: String, role: Constants.RegEx)
}

class ErrorManager {
    
    func checkErrorType(element: String, role: Constants.RegEx, elementName: String = "") -> String {
        guard !element.isEmpty else { return "\(elementName) cannot be empty" }
        guard element.isValid(role) else { return "\(elementName) is not valid" }
        // TODO: Sweet checking of error type
        return "This should not have happened but it happened" // "Этого не должно было произойти но произошло"
    }
}
