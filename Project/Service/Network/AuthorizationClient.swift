//
//  AuthorizationClient.swift
//  Project
//
//  Created by Igor Ratynski on 24/01/2019.
//  Copyright © 2019 Igor Ratynski. All rights reserved.
//

import Foundation

class AuthorizationClient: APIClient, AuthorizationApiProtocol {

    let session: URLSession

    init(configuration: URLSessionConfiguration) {
        self.session = URLSession(configuration: configuration)
    }

    convenience init() {
        self.init(configuration: .default)
    }
    //swiftlint:disable next line_length
    func request(type: AuthorizationType, email: String, password: String, completion: @escaping (Result<ProfileModelResult?, APIError>) -> Void) {
        let request = Factory.construct(element: .authorization(type: type, email: email, password: password)) // TODO: Refactoring task: 0x1
        fetch(with: request as URLRequest, decode: { json -> ProfileModelResult? in
            return json as? ProfileModelResult
        }, completion: completion)
    }
    //swiftlint:enable next line_length
}
