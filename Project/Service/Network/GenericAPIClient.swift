//
//  GenericAPIClient.swift
//  Project
//
//  Created by Igor Ratynski on 24/01/2019.
//  Copyright © 2019 Igor Ratynski. All rights reserved.
//

import Foundation

enum APIError: Error {
    case requestFailed
    case jsonConversionFailure
    case invalidData
    case responseUnsuccessful
    case jsonParsingFailure

    var localizedDescription: String {
        switch self {
        case .requestFailed: return "Request Failed"
        case .invalidData: return "Invalid Data"
        case .responseUnsuccessful: return "Response Unsuccessful"
        case .jsonParsingFailure: return "JSON Parsing Failure"
        case .jsonConversionFailure: return "JSON Conversion Failure"
        }
    }
}

protocol APIClient {

    var session: URLSession { get }
    func fetch<T: Decodable>(with request: URLRequest, decode: @escaping (Decodable) -> T?, completion: @escaping (Result<T, APIError>) -> Void)

}

extension APIClient {

    typealias JSONTaskCompletionHandler = (Decodable?, APIError?) -> Void
    //swiftlint:disable next line_length
    func decodingTask<T: Decodable>(with request: URLRequest, decodingType: T.Type, completionHandler completion: @escaping JSONTaskCompletionHandler) -> URLSessionDataTask {
    //swiftlint:enable next line_length
        let task = session.dataTask(with: request) { data, response, _ in

            guard let httpResponse = response as? HTTPURLResponse else {
                completion(nil, .requestFailed)
                return
            }
            guard httpResponse.statusCode == 200 else {
                completion(nil, .responseUnsuccessful)
                return
            }

            guard let data = data else {
                completion(nil, .invalidData)
                return
            }

            do {
                let profile = try JSONDecoder(decodingStrategy: .convertFromSnakeCase).decode(decodingType, from: data)
                completion(profile, nil)
            } catch {
                completion(nil, .jsonConversionFailure)
            }
        }
        return task
    }

    func fetch<T: Decodable>(with request: URLRequest, decode: @escaping (Decodable) -> T?, completion: @escaping (Result<T, APIError>) -> Void) {

        let task = decodingTask(with: request, decodingType: T.self) { (json, error) in

            // MARK: change to main queue
            DispatchQueue.main.async {
                guard let json = json else {
                    if let error = error {
                        completion(Result.failure(error))
                    } else {
                        completion(Result.failure(.invalidData))
                    }
                    return
                }
                if let value = decode(json) {
                    completion(.success(value))
                } else {
                    completion(.failure(.jsonParsingFailure))
                }
            }
        }
        task.resume()
    }
}

private extension JSONDecoder {
    convenience init(decodingStrategy: KeyDecodingStrategy) {
        self.init()
        keyDecodingStrategy = .convertFromSnakeCase
    }
}
