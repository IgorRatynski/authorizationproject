//
//  Result.swift
//  Project
//
//  Created by Igor Ratynski on 24/01/2019.
//  Copyright © 2019 Igor Ratynski. All rights reserved.
//

import Foundation

enum Result<T, U> where U: Error {
    case success(T)
    case failure(U)
}
