//
//  Endpoint.swift
//  Project
//
//  Created by Igor Ratynski on 24/01/2019.
//  Copyright © 2019 Igor Ratynski. All rights reserved.
//

import Foundation

protocol Endpoint {
    var base: String { get }
    var path: String { get }
}

extension Endpoint {
    
    var urlComponents: URLComponents {
        var components = URLComponents(string: base)!
        components.path = path
        return components
    }
    
    // TODO: Refactoring task: 0x2
    var loginRequest: URLRequest {
        let url = URL(string: urlComponents.url!.absoluteString.removingPercentEncoding!)!
        return URLRequest(url: url)
    }
    var registrationRequest: URLRequest {
        let url = urlComponents.url!
        return URLRequest(url: url)
    }
    // TODO: Refactoring task: 0x2
}

enum AuthorizationType {
    case registration
    case login(_ email: String, _ password: String)
}

extension AuthorizationType: Endpoint {

    var base: String {
        return "http://api.freeworker.pro"
    }
    // swiftlint:disable next line_length
    var path: String {
        switch self {
        case .registration: return "/authorization/registration"
        case .login(let email, let password): return Factory.construct(element: .loginURL(queryItems: ["email": email, "password": password])).absoluteString // TODO: Refactoring task: 0x1
        }
    }
    //swiftlint:enable next line_length
}
