//
//  Router.swift
//  Project
//
//  Created by Igor Ratynski on 20/01/2019.
//  Copyright © 2019 Igor Ratynski. All rights reserved.
//

import UIKit

extension UIViewController {

    enum Routing {
        case login
        case registration
        case profile
    }

    enum ViewType {
        case undefined

        case login
        case registration
        case profile
    }

    private struct Keys {
        static var key = "\(#file)+\(#line)"
    }

    var type: ViewType {
        get { return objc_getAssociatedObject(self, &Keys.key) as? ViewType ?? .undefined }
        set { objc_setAssociatedObject(self, &Keys.key, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
    }

    func routing(with routing: Routing) {
        switch type {

        case .login:
            login(with: routing)

        case .registration:
            registration(with: routing)

        case .profile:
            profile(with: routing)

        default: break
        }
    }
}

private extension UIViewController {

    func login(with routing: Routing) {
        switch routing {

        case .registration:
            navigationController?.pushViewController(Factory.constructScreen(type: .registration)!, animated: true)
        case .profile:
            present(Factory.constructEmbededScreen(type: .profile)!, animated: true)

        default:break
        }
    }

    func registration(with routing: Routing) {
        switch routing {

        case .login:
            navigationController?.popViewController(animated: false)
        case .profile:
            present(Factory.constructEmbededScreen(type: .profile)!, animated: true)

        default:break
        }
    }

    func profile(with routing: Routing) {
        switch routing {

        case .login:
            dismiss(animated: true, completion: nil)

        default:break
        }
    }

}
