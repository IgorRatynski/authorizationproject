//
//  Factory.swift
//  Project
//
//  Created by Igor Ratynski on 23/01/2019.
//  Copyright © 2019 Igor Ratynski. All rights reserved.
//

import UIKit

// swiftlint:disable next nesting
class Factory {

    enum Construct {

        // MARK: - UI

        enum Screen {

            enum Authorization {
                case login
                case registration
            }

            enum Content {
                case profile
            }
        }

        enum Label {
            case `default`(String)
        }
        enum Button {
            case `default`(String)
        }
        enum TextView {
            case `default`
        }
        enum BarButton {
            case `default`(target: AnyObject, selector: Selector, title: String)
        }
        enum TextField {
            case `default`(String)
        }
        enum ImageView {
            case profile
            case `default`
        }
        enum StackView {
            case `default`([UIView])
        }
        enum ScrollView {
            case `default`
        }

        enum StackViewRowPair {
            case label(with: String, value: UIView)
        }

        // MARK: - Model

        enum RegEx {
            case `default`(String)
        }

        enum RequestURL {
            case loginURL(queryItems: [String : String])
        }

        enum Request {
            case authorization(type: AuthorizationType, email: String, password: String)
        }
    }
}
// swiftlint:enable next nesting
