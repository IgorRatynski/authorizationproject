//
//  FactoryScreen.swift
//  Project
//
//  Created by Igor Ratynski on 31/01/2019.
//  Copyright © 2019 Igor Ratynski. All rights reserved.
//

import UIKit

extension Factory: FactoryScreenProtocol {

    typealias Screen = Construct.Screen

    enum ScreenType: String {
        case login = "LoginViewController"
        case registration = "RegistrationViewController"
        case profile = "ProfileViewController"
    }

    static func constructScreen(type: ScreenType) -> UIViewController? {
        guard let viewController: UIViewController.Type = type.rawValue.convertToClass() else {
            return nil
        }
        return viewController.init()
    }

    static func constructEmbededScreen(type: ScreenType) -> UINavigationController? {
        return UINavigationController(rootViewController: constructScreen(type: type)!)
    }

    // 2-d way

    var login: LoginViewController { return LoginViewController() }
    var registration: RegistrationViewController { return RegistrationViewController() }
    var profile: ProfileViewController { return ProfileViewController() }
}
