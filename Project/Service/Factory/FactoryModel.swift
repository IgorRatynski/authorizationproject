//
//  FactoryModel.swift
//  Project
//
//  Created by Igor Ratynski on 20/01/2019.
//  Copyright © 2019 Igor Ratynski. All rights reserved.
//

import Foundation

extension Factory: FactoryModelProtocol {

    // ignore `try!` because all regex are our
    // swiftlint:disable next force_try
    static func construct(element: Construct.RegEx) -> NSRegularExpression {
        switch element {
        case .default(let regex):
            return try! NSRegularExpression(pattern: regex, options: .caseInsensitive)
        }
    }
    // swiftlint:enable next force_try
}

extension Factory: FactoryNetworkProtocol {

    typealias Request = Construct.Request
    typealias RequestURL = Construct.RequestURL
    
    static func construct(element: RequestURL) -> URL {
        switch element {
        case .loginURL(let queryItemsDict):
            let urlComps = NSURLComponents(string: "/authorization/login")!
            urlComps.queryItems = queryItemsDict.compactMap { URLQueryItem(name: $0.key, value: $0.value) }
            return urlComps.url!
        }
    }

    static func construct(element: Request) -> URLRequest {
        switch element {

        case .authorization(type: let endpoint, email: let email, password: let password):
            var request: URLRequest

            switch endpoint {
            case .login:
                request = endpoint.loginRequest
                request.httpMethod = "GET"

            case .registration:
                request = endpoint.registrationRequest
                request.httpMethod = "POST"
                let parameters: [String: Any] = [
                    Keys.Common.email: email,
                    Keys.Common.password: password
                ]

                if let registrationDataJSON = try? JSONSerialization.data(withJSONObject: parameters, options: []) {
                    request.httpBody = registrationDataJSON
                }
            }

            request.addValue("Android", forHTTPHeaderField: "Audience")
            request.addValue("mobile", forHTTPHeaderField: "User-Agent")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")

            return request
        }
    }
}
