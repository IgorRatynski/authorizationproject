//
//  Factory.swift
//  Project
//
//  Created by Igor Ratynski on 18/01/2019.
//  Copyright © 2019 Igor Ratynski. All rights reserved.
//

import UIKit

extension Factory: FactoryUIProtocol {

    typealias Label = Construct.Label
    typealias Button = Construct.Button
    typealias TextView = Construct.TextView
    typealias BarButton = Construct.BarButton
    typealias TextField = Construct.TextField
    typealias ImageView = Construct.ImageView
    typealias StackView = Construct.StackView
    typealias ScrollView = Construct.ScrollView
    typealias StackViewRowPair = Construct.StackViewRowPair

    static func construct(element: Label) -> UILabel {
        switch element {
        case .default(let title):
            return UILabel(title: title)
        }
    }
    static func construct(element: Button) -> UIButton {
        switch element {
        case .default(let title):
            return UIButton(title: title)
        }
    }
    static func construct(element: BarButton) -> UIBarButtonItem {
        switch element {
        case .default(target: let target, selector: let selector, title: let title):
            return UIBarButtonItem(title: title, style: .plain, target: target, action: selector)
        }
    }
    static func construct(element: TextView) -> UITextView {
        switch element {
        case .default:
            return UITextView(preparedForResizing: true)
        }
    }
    static func construct(element: TextField) -> UITextField {
        switch element {
        case .default(let placeholder):
            return UITextField(placeholder: placeholder)
        }
    }
    static func construct(element: ImageView) -> UIImageView {
        switch element {
        case .profile:
            return UIImageView(size: UIScreen.halfWidthSize)
        case .default:
            return UIImageView(preparedForResizing: true)
        }
    }
    static func construct(element: StackView) -> UIStackView {
        switch element {
        case .default(let subviews):
            return UIStackView(subviews: subviews)
        }
    }
    static func construct(element: ScrollView) -> UIScrollView {
        switch element {
        case .default:
            return UIScrollView(preparedForResizing: true)
        }
    }

    static func construct(element: StackViewRowPair) -> UIView {
        switch element {
        case .label(with: let title, value: let view):
            let baseView = UIView(preparedForResizing: true)
            let titleLabel = UILabel(title: title)

            baseView.addSubview(titleLabel)
            baseView.addSubview(view)
            titleLabel.stickToLeftSide()
            view.stickToRightSide()
            titleLabel.rightAnchor.constraint(equalTo: view.leftAnchor).isActive = true

            return baseView
        }
    }
}

// MARK: - Constructor

private extension UIButton {
    convenience init(title: String) {
        self.init(type: .system)
        setTitle(title, for: .normal)
    }
}

private extension UIImageView {
    convenience init(size: CGFloat) {
        self.init(preparedForResizing: true)
        widthAnchor.constraint(equalToConstant: size).isActive = true
        heightAnchor.constraint(equalToConstant: size).isActive = true
        //
        clipsToBounds = true
        layer.cornerRadius = size / 2
        image = UIImage(named: "profile-no-image-white")
        contentMode = .scaleAspectFit
    }
}

private extension UIView {
    convenience init(preparedForResizing: Bool) {
        self.init()
        translatesAutoresizingMaskIntoConstraints = false
    }
}

private extension UITextField {
    convenience init(placeholder text: String?) {
        self.init(preparedForResizing: true)
        borderStyle = .roundedRect
        attributedPlaceholder = NSAttributedString(text: text ?? "")
        textAlignment = .center
    }
}

private extension UIStackView {
    convenience init(subviews: [UIView]) {
        self.init(arrangedSubviews: subviews)
        axis = .vertical
        alignment = .center
        distribution = .fill // .equalSpacing
        contentMode = .scaleAspectFit
        translatesAutoresizingMaskIntoConstraints = false
        spacing = 5
    }
}

private extension UILabel {
    convenience init(title: String) {
        self.init(preparedForResizing: true)
        text = title
    }
}

// MARK: - Helper methods

private extension UIView {
    func stickToLeftSide() {
        guard superview != nil else { fatalError("Superview cannot be nil") }
        topAnchor.constraint(equalTo: superview!.topAnchor).isActive = true
        leftAnchor.constraint(equalTo: superview!.leftAnchor).isActive = true
        bottomAnchor.constraint(equalTo: superview!.bottomAnchor).isActive = true
    }
    func stickToRightSide() {
        guard superview != nil else { fatalError("Superview cannot be nil") }
        topAnchor.constraint(equalTo: superview!.topAnchor).isActive = true
        rightAnchor.constraint(equalTo: superview!.rightAnchor).isActive = true
        bottomAnchor.constraint(equalTo: superview!.bottomAnchor).isActive = true
    }
}

private extension UIScreen {
    static var halfWidthSize: CGFloat {
        return UIScreen.main.bounds.width / 2
    }
}

private extension NSAttributedString {
    convenience init(text: String) {
        let centeredParagraphStyle = NSMutableParagraphStyle()
        centeredParagraphStyle.alignment = .center
        self.init(string: text, attributes: [NSAttributedString.Key.paragraphStyle: centeredParagraphStyle])
    }
}
