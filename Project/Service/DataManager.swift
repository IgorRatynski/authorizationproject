//
//  DataManager.swift
//  Project
//
//  Created by Igor Ratynski on 01/02/2019.
//  Copyright © 2019 Igor Ratynski. All rights reserved.
//

import Foundation

class DataManager {

    static let shared = DataManager()

    var accessToken: String?
    var profile: ProfilePayload?

    func set(profileModel: ProfileModel) {
        accessToken = profileModel.accessToken
        profile = profileModel.payload
    }
}
