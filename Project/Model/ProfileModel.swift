//
//  ProfileModel.swift
//  Project
//
//  Created by Igor Ratynski on 22/01/2019.
//  Copyright © 2019 Igor Ratynski. All rights reserved.
//

import Foundation

struct ProfileModelResult: Decodable {
    let response: ProfileModel
}

struct ProfileModel: Decodable {
    let accessToken: String
    let payload: ProfilePayload
}

struct ProfilePayload: Decodable {
    let email: String
    let phoneNumber: String
    let phoneNumberVerified: Bool
    let name: String
    let familyName: String
    let givenName: String
    let nickname: String
    let about: String
    let emailVerified: Bool
    let picture: String
    let background: String
    let userId: Int
    let city: String
    let adr: String
    let lat: Double
    let lon: Double
}
