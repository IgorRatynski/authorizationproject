//
//  ProfilePresenter.swift
//  Project
//
//  Created by Igor Ratynski on 31/01/2019.
//  Copyright © 2019 Igor Ratynski. All rights reserved.
//

import UIKit

class ProfilePresenter: ProfilePresenterProtocol {

    // MARK: - Private properties

    private weak var view: ProfileViewProtocol?
    private var isEditing = false

    // MARK: - Public

    func attach(view: ProfileViewProtocol) {
        self.view = view
    }

    func setupModel() {
        view?.nicknameTextField.text = DataManager.shared.profile?.nickname
        view?.nameTextField.text = DataManager.shared.profile?.name
        view?.givenNameTextField.text = DataManager.shared.profile?.givenName
        view?.familyNameTextField.text = DataManager.shared.profile?.familyName
        view?.emailTextField.text = DataManager.shared.profile?.email
        view?.cityTextField.text = DataManager.shared.profile?.city
        view?.adressTextField.text = DataManager.shared.profile?.adr
        view?.latitudeLabel.text = String(DataManager.shared.profile!.lat)
        view?.longitudeLabel.text = String(DataManager.shared.profile!.lon)
        view?.phoneNumberTextField.text = DataManager.shared.profile?.phoneNumber
        view?.aboutTextView.text = DataManager.shared.profile?.about
    }

    func setupState() {
        view?.nicknameTextField.set(state: isEditing)
        view?.nameTextField.set(state: isEditing)
        view?.givenNameTextField.set(state: isEditing)
        view?.familyNameTextField.set(state: isEditing)
        view?.emailTextField.set(state: isEditing)
        view?.cityTextField.set(state: isEditing)
        view?.adressTextField.set(state: isEditing)
        view?.phoneNumberTextField.set(state: isEditing)
    }
}

private extension UITextField {
    func set(state: Bool) {
        borderStyle = state ? .roundedRect : .none
        isUserInteractionEnabled = state
    }
}
