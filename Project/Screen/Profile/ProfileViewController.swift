//
//  ProfileViewController.swift
//  Project
//
//  Created by Igor Ratynski on 31/01/2019.
//  Copyright © 2019 Igor Ratynski. All rights reserved.
//

import UIKit

// swiftlint:disable next force_cast, line_length
class ProfileViewController: UIViewController, ProfileViewProtocol {

    // MARK: - Public properties

    let containerScrollView: UIScrollView = Factory.construct(element: ScrollView.default)

    var constainerStackView: UIStackView!
    let backgroundImageView: UIImageView = Factory.construct(element: ImageView.default)

    let profileImageView: UIImageView = Factory.construct(element: ImageView.profile)

    let nicknameTextField: UITextField = Factory.construct(element: TextField.default("Nickname"))
    let phoneNumberTextField: UITextField = Factory.construct(element: TextField.default("Please enter phone"))
    let nameTextField: UITextField = Factory.construct(element: TextField.default("Name"))
    let familyNameTextField: UITextField = Factory.construct(element: TextField.default("Family"))
    let givenNameTextField: UITextField = Factory.construct(element: TextField.default("Given name"))
    let emailTextField: UITextField = Factory.construct(element: TextField.default("E-mail"))

    let cityTextField: UITextField = Factory.construct(element: TextField.default("Please enter city"))
    let adressTextField: UITextField = Factory.construct(element: TextField.default("Please enter address"))
    let latitudeLabel: UILabel = Factory.construct(element: Label.default("31.43875839"))
    let longitudeLabel: UILabel = Factory.construct(element: Label.default("50.43875839"))

    let aboutTextView: UITextView = Factory.construct(element: TextView.default)

    let passwordTextField: UITextField = Factory.construct(element: TextField.default("Password"))
    lazy var repeatPasswordTextField: UITextField = {
        return Factory.construct(element: TextField.default("Repeat password"))
    }()

    // MARK: - Private properties

    private let presenter: ProfilePresenterProtocol = ProfilePresenter()
    private weak var currentView: UIView!
    private var userIdLabel: UILabel!

    // MARK: - Action

    @objc private func backToLogin() {
        routing(with: .login)
    }

    // MARK: - UIViewController lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    // MARK: - Private methods

    private func setup() {
        type = .profile // name of current screen for routing
        title = "Profile"
        presenter.attach(view: self)
        addInterface()
        view.backgroundColor = UIColor.white
    }

    private func addInterface() {
        addBackButton()
        initStackView()
        addConstratints()
        presenter.setupModel()
        presenter.setupState()
    }

    private func addBackButton() {
        navigationItem.leftBarButtonItem = Factory.construct(element: BarButton.default(target: self, selector: #selector(backToLogin), title: "Back"))
    }

    private func initStackView() {
        let nicknameRow = Factory.construct(element: StackViewRowPair.label(with: "Nickname:", value: nicknameTextField))
        let nameRow = Factory.construct(element: StackViewRowPair.label(with: "Name:", value: nameTextField))
        let givenNameRow = Factory.construct(element: StackViewRowPair.label(with: "Given name:", value: givenNameTextField))
        let familyRow = Factory.construct(element: StackViewRowPair.label(with: "Family:", value: familyNameTextField))
        let emailRow = Factory.construct(element: StackViewRowPair.label(with: "E-mail:", value: emailTextField))

        let cityRow = Factory.construct(element: StackViewRowPair.label(with: "City:", value: cityTextField))
        let adressRow = Factory.construct(element: StackViewRowPair.label(with: "Adress:", value: adressTextField))
        let latitudeRow = Factory.construct(element: StackViewRowPair.label(with: "Latitude:", value: latitudeLabel))
        let longitudeRow = Factory.construct(element: StackViewRowPair.label(with: "Longitude:", value: longitudeLabel))
        let phoneNumberRow = Factory.construct(element: StackViewRowPair.label(with: "Phone number:", value: phoneNumberTextField))
        let aboutRow = Factory.construct(element: StackViewRowPair.label(with: "About:", value: aboutTextView))

        let subviews: [UIView] = [profileImageView, nicknameRow, nameRow, givenNameRow, familyRow, emailRow, cityRow, adressRow, latitudeRow, longitudeRow, phoneNumberRow, aboutRow]
        constainerStackView = Factory.construct(element: StackView.default(subviews))
    }

    private func addConstratints() {
        view.addSubview(containerScrollView)
        containerScrollView.addSubview(backgroundImageView)
        backgroundImageView.addSubview(constainerStackView)

        containerScrollView.connectToBounds(of: view)
        backgroundImageView.connectToBounds(of: containerScrollView)

        backgroundImageView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true

        constainerStackView.topAnchor.constraint(equalTo: backgroundImageView.topAnchor, constant: 20).isActive = true
        constainerStackView.bottomAnchor.constraint(equalTo: backgroundImageView.bottomAnchor, constant: -20).isActive = true
        constainerStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        constainerStackView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.95).isActive = true
    }

    // MARK: - Shorting typealias for factory

    typealias Label = Factory.Construct.Label
    typealias Button = Factory.Construct.Button
    typealias TextView = Factory.Construct.TextView
    typealias BarButton = Factory.Construct.BarButton
    typealias TextField = Factory.Construct.TextField
    typealias ImageView = Factory.Construct.ImageView
    typealias StackView = Factory.Construct.StackView
    typealias ScrollView = Factory.Construct.ScrollView
    typealias StackViewRowPair = Factory.Construct.StackViewRowPair
}

private extension UIView {
    func connectToBounds(of view: UIView) {
        topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
}
// swiftlint:ebable next force_cast, line_length
