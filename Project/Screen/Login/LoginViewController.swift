//
//  LoginViewController.swift
//  Project
//
//  Created by Igor Ratynski on 17/01/2019.
//  Copyright © 2019 Igor Ratynski. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, LoginViewProtocol {

    // MARK: - Public properties

    let emailTextField = Factory.construct(element: TextField.default("E-mail"))
    let passwordTextField = Factory.construct(element: TextField.default("Password"))

    // MARK: - Private properties

    private let loginButton = Factory.construct(element: Button.default("Login"))
    private let registrationButton = Factory.construct(element: Button.default("Registration"))
    private let presenter: LoginPresenterProtocol = LoginPresenter() // : LoginPresenterProtocol

    // MARK: - Action

    @objc private func loginAction() {
        presenter.login()
    }

    @objc private func showRegistration() {
        routing(with: .registration)
    }

    // MARK: - UIViewController lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    // MARK: - Private methods

    private func setup() {
        view.backgroundColor = UIColor.white
        type = .login // name of current screen for routing
        title = "Login"
        presenter.attach(view: self)
        addInterface()
        addHideKeyboardAction()
    }

    private func addInterface() {
        addButtonAction()
        initStackView()
    }

    private func addButtonAction() {
        loginButton.addTarget(self, action: #selector(loginAction), for: .touchUpInside)
        registrationButton.addTarget(self, action: #selector(showRegistration), for: .touchUpInside)
    }

    private func initStackView() {
        let subviews: [UIView] = [emailTextField, passwordTextField, loginButton, registrationButton]
        let stackView = Factory.construct(element: StackView.default(subviews))
        addSubview(stackView)
    }

    private func addHideKeyboardAction() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tap)
    }

    @objc private func hideKeyboard() {
        view.endEditing(false)
    }

    private func addSubview(_ subview: UIView) {
        view.addSubview(subview)
        addConstratints(subview)
        emailTextField.setWidthEqualSuperview()
        passwordTextField.setWidthEqualSuperview()
    }

    private func addConstratints(_ subview: UIView) {
        subview.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        subview.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        subview.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.7).isActive = true
    }

    // MARK: - Shorting typealias for factory

    typealias Button = Factory.Construct.Button
    typealias TextField = Factory.Construct.TextField
    typealias StackView = Factory.Construct.StackView
}
