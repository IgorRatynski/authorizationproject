//
//  LoginPresenter.swift
//  Project
//
//  Created by Igor Ratynski on 20/01/2019.
//  Copyright © 2019 Igor Ratynski. All rights reserved.
//

import Foundation
import UIKit

class LoginPresenter: LoginPresenterProtocol {

    // MARK: - Private properties

    private weak var view: LoginViewProtocol?
    private let api: AuthorizationApiProtocol = AuthorizationClient()

    // MARK: - Public

    func attach(view: LoginViewProtocol) {
        self.view = view
    }

    func login() {
        guard isInputValid() else {
            handleInputError()
            return
        }

        let email = view!.emailTextField.text!
        let password = view!.passwordTextField.text!

        api.request(type: AuthorizationType.login(email, password), email: email, password: password) { [weak self] result in
            switch result {
            case .success(let registrationResult):
                guard let profileModel = registrationResult?.response else { return }
                DataManager.shared.set(profileModel: profileModel)
                self?.view?.routing(with: .profile) // TODO: Transition end action: clear fields
            case .failure(let error):
                self?.view?.presentAlertWith(title: "Error", message: "The error \(error)")
            }
        }

    }

    // MARK: - Private

    private func isInputValid() -> Bool {
        guard let emailString = view?.emailTextField.text,
            let passwordString = view?.passwordTextField.text else {
                return false
        }

        return emailString.isValid(.email) && passwordString.isValid(.password)
    }

    private func handleInputError() {
        guard let email = view?.emailTextField.text,
            let password = view?.passwordTextField.text,
            handleErrorIn(email, role: .email, elementName: "E-mail"),
            handleErrorIn(password, role: .password, elementName: "Password") else {
                return
        }
    }

    private func handleErrorIn(_ string: String?, role: Constants.RegEx, elementName: String) -> Bool {
        guard string?.isValid(role) ?? false else {
            let errorTitle = ErrorManager().checkErrorType(element: string ?? "", role: role)
            view?.presentAlertWith(title: "Error!", message: errorTitle)
            return false
        }
        return true
    }

}
