//
//  RegistrationPresenter.swift
//  Project
//
//  Created by Igor Ratynski on 31/01/2019.
//  Copyright © 2019 Igor Ratynski. All rights reserved.
//

import Foundation
import UIKit

class RegistrationPresenter: RegistrationPresenterProtocol {

    // MARK: - Private properties

    private weak var view: RegistrationViewProtocol?
    private let api: AuthorizationApiProtocol = AuthorizationClient()

    // MARK: - Public

    func attach(view: RegistrationViewProtocol) {
        self.view = view
    }

    func registration() {
        guard isInputValid() else {
            handleInputError()
            return
        }
        let email = view!.emailTextField.text!
        let password = view!.passwordTextField.text!

        api.request(type: AuthorizationType.registration, email: email, password: password) { [weak self] result in
            switch result {
            case .success(let registrationResult):
                guard let profileModel = registrationResult?.response else { return }
                DataManager.shared.set(profileModel: profileModel)
                self?.view?.routing(with: .profile) // TODO: Transition end action: clear fields
            case .failure(let error):
                self?.view?.presentAlertWith(title: "Error", message: "The error \(error)")
            }
        }
    }

    // MARK: - Private

    private func isInputValid() -> Bool {
        guard let emailString = view?.emailTextField.text,
            let passwordString = view?.passwordTextField.text,
            let repeatPasswordString = view?.repeatPasswordTextField.text else {
                return false
        }
        return emailString.isValid(.email) && passwordString.elementsEqual(repeatPasswordString) && passwordString.isValid(.password)
    }

    private func handleInputError() {

        guard let email = view?.emailTextField.text,
            let password = view?.passwordTextField.text,
            handleErrorIn(email, role: .email, elementName: "E-mail"),
            checkPasswordsEqual(),
            handleErrorIn(password, role: .password, elementName: "Password") else {
            return
        }
    }

    private func handleErrorIn(_ string: String?, role: Constants.RegEx, elementName: String) -> Bool {
        guard string?.isValid(role) ?? false else {
            let errorTitle = ErrorManager().checkErrorType(element: string ?? "", role: role, elementName: elementName)
            view?.presentAlertWith(title: "Error!", message: errorTitle)
            return false
        }
        return true
    }

    private func checkPasswordsEqual() -> Bool {
        guard let passwordString = view?.passwordTextField.text,
            let repeatPasswordString = view?.repeatPasswordTextField.text,
            passwordString.elementsEqual(repeatPasswordString) else {
                view?.presentAlertWith(title: "Error!", message: "Passwords are not equal")
                return false
        }
        return true
    }

}
