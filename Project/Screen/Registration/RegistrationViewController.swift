//
//  RegistrationViewController.swift
//  Project
//
//  Created by Igor Ratynski on 31/01/2019.
//  Copyright © 2019 Igor Ratynski. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController, RegistrationViewProtocol {

    // MARK: - Public properties

    let emailTextField = Factory.construct(element: TextField.default("E-mail"))
    let passwordTextField = Factory.construct(element: TextField.default("Password"))
    let repeatPasswordTextField = Factory.construct(element: TextField.default("Repeat password"))

    // MARK: - Private properties
    
    private let registrationButton = Factory.construct(element: Button.default("Registration"))
    private let presenter: RegistrationPresenterProtocol = RegistrationPresenter()

    // MARK: - Action

    @objc private func registrationButtonTapped() {
        presenter.registration()
    }

    // MARK: - UIViewController lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    // MARK: - Private methods

    private func setup() {
        type = .registration
        title = "Registration"
        presenter.attach(view: self)
        addInterface()
        view.backgroundColor = UIColor.white
    }

    private func addInterface() {
        addButtonAction()
        setupStackView()
        addHideKeyboardAction()
    }

    private func addButtonAction() {
        registrationButton.addTarget(self, action: #selector(registrationButtonTapped), for: .touchUpInside)
    }

    private func setupStackView() {
        let subviews: [UIView] = [emailTextField, passwordTextField, repeatPasswordTextField, registrationButton]
        let stackView = Factory.construct(element: StackView.default(subviews))
        addSubview(stackView)
    }

    private func addHideKeyboardAction() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tap)
    }

    @objc private func hideKeyboard() {
        view.endEditing(false)
    }

    private func addSubview(_ subview: UIView) {
        view.addSubview(subview)
        addConstratints(subview)
    }

    private func addConstratints(_ subview: UIView) {
        emailTextField.setWidthEqualSuperview()
        passwordTextField.setWidthEqualSuperview()
        repeatPasswordTextField.setWidthEqualSuperview()
        subview.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        subview.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        subview.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.7).isActive = true
    }

    // MARK: - Shorting typealias for factory

    typealias Button = Factory.Construct.Button
    typealias TextField = Factory.Construct.TextField
    typealias StackView = Factory.Construct.StackView
}
