//
//  FactoryProtocol.swift
//  Project
//
//  Created by Igor Ratynski on 18/01/2019.
//  Copyright © 2019 Igor Ratynski. All rights reserved.
//

import UIKit

typealias Construct = Factory.Construct

// MARK: - UI

protocol FactoryScreenProtocol {
    static func constructScreen(type: Factory.ScreenType) -> UIViewController?
}

protocol FactoryUIProtocol {
    static func construct(element: Construct.Label) -> UILabel
    static func construct(element: Construct.Button) -> UIButton
    static func construct(element: Construct.TextView) -> UITextView
    static func construct(element: Construct.BarButton) -> UIBarButtonItem
    static func construct(element: Construct.TextField) -> UITextField
    static func construct(element: Construct.ImageView) -> UIImageView
    static func construct(element: Construct.StackView) -> UIStackView
    static func construct(element: Construct.ScrollView) -> UIScrollView
    static func construct(element: Construct.StackViewRowPair) -> UIView
}

// MARK: - Model

protocol FactoryModelProtocol {
    static func construct(element: Construct.RegEx) -> NSRegularExpression
}

protocol FactoryNetworkProtocol {
    static func construct(element: Construct.RequestURL) -> URL
}
