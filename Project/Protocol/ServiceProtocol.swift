//
//  ServiceProtocol.swift
//  Project
//
//  Created by Igor Ratynski on 22/01/2019.
//  Copyright © 2019 Igor Ratynski. All rights reserved.
//

import Foundation

protocol AuthorizationApiProtocol {
    func request(type: AuthorizationType, email: String, password: String, completion: @escaping (Result<ProfileModelResult?, APIError>) -> Void)
}
