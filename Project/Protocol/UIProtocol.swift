//
//  ViewProtocol.swift
//  Project
//
//  Created by Igor Ratynski on 20/01/2019.
//  Copyright © 2019 Igor Ratynski. All rights reserved.
//

import UIKit

// MARK: - View protocol

protocol InputInterfaceCheckerProtocol: class {
    func isInputValid() -> Bool
    func action()
}

protocol MVPViewProtocol: class {
    func routing(with routing: UIViewController.Routing)
}

protocol LoginViewProtocol: MVPViewProtocol, AlertViewProtocol {
    var emailTextField: UITextField { get }
    var passwordTextField: UITextField { get }
}

protocol RegistrationViewProtocol: MVPViewProtocol, AlertViewProtocol {
    var emailTextField: UITextField { get }
    var passwordTextField: UITextField { get }
    var repeatPasswordTextField: UITextField { get }
}

protocol ProfileViewProtocol: MVPViewProtocol {
    var backgroundImageView: UIImageView { get }
    var profileImageView: UIImageView { get }
    var phoneNumberTextField: UITextField { get }
    var nameTextField: UITextField { get }
    var familyNameTextField: UITextField { get }
    var givenNameTextField: UITextField { get }
    var nicknameTextField: UITextField { get }
    var aboutTextView: UITextView { get }
    var cityTextField: UITextField { get }
    var adressTextField: UITextField { get }
    var latitudeLabel: UILabel { get }
    var longitudeLabel: UILabel { get }
    var emailTextField: UITextField { get }
    var passwordTextField: UITextField { get }
    var repeatPasswordTextField: UITextField { get }
}

// MARK: - View extension protocol

protocol AlertViewProtocol {
    func presentAlertWith(title: String, message: String)
}
