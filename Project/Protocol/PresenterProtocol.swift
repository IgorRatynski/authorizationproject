//
//  PresenterProtocol.swift
//  Project
//
//  Created by Igor Ratynski on 20/01/2019.
//  Copyright © 2019 Igor Ratynski. All rights reserved.
//

import UIKit

protocol LoginPresenterProtocol {
    func attach(view: LoginViewProtocol)
    func login()
}

protocol RegistrationPresenterProtocol {
    func attach(view: RegistrationViewProtocol)
    func registration()
}

protocol ProfilePresenterProtocol {
    func attach(view: ProfileViewProtocol)
    func setupModel()
    func setupState()
}
