//
//  Constants.swift
//  Project
//
//  Created by Igor Ratynski on 20/01/2019.
//  Copyright © 2019 Igor Ratynski. All rights reserved.
//

import Foundation

enum Constants {
    enum RegEx: String, CustomStringConvertible {
        // swiftlint:disable next line_length
        case email = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        case password = "^(?=.*[A-Za-z])(?=.*[0-9])(?!.*[^A-Za-z0-9]).{3,15}$"
        // swiftlint:enable next line_length
        var description: String { return self.rawValue }
    }

}

enum Keys {
    typealias RawValue = String

    enum Common {
        static var email: String { return "email" }
        static var password: String { return "password" }
    }

    enum Network {
        static var audience: String { return "Audience" }
        static var userAgent: String { return "User-Agent" }
        static var contentType: String { return "Content-Type" }
        static var baseURL: String { return "http://api.freeworker.pro/authorization" }
    }

    enum Screen {
        static var login: String { return "LoginViewController" }
        static var registration: String { return "RegistrationViewController" }
        static var profile: String { return "ProfileViewController" }
    }

}

// MARK: - Regular Expression

/// - Password
///
///^                         Start anchor
///(?=.*[A-Z].*[A-Z])        Ensure string has two uppercase letters.
///(?=.*[!@#$&*])            Ensure string has one special case letter.
///(?=.*[0-9].*[0-9])        Ensure string has two digits.
///(?=.*[a-z].*[a-z].*[a-z]) Ensure string has three lowercase letters.
///.{8}                      Ensure string is of length 8.
///$                         End anchor.

// swiftlint defauld bould script
