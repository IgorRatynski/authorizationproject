//
//  Extension.swift
//  Project
//
//  Created by Igor Ratynski on 20/01/2019.
//  Copyright © 2019 Igor Ratynski. All rights reserved.
//

import UIKit

// MARK: - Foundation

/// Class from string
/// https://stackoverflow.com/a/48867619/4742680 - if app name have spaces
extension String {
    func convertToClass<T>() -> T.Type? {
        return StringClassConverter<T>.convert(string: self)
    }
}

extension Bundle {
    // Name of the app - title under the icon.
    var displayName: String? {
        return object(forInfoDictionaryKey: "CFBundleDisplayName") as? String ??
            object(forInfoDictionaryKey: "CFBundleName") as? String
    }
}

class StringClassConverter<T> {
    static func convert(string className: String) -> T.Type? {
        guard let nameSpace = Bundle.main.infoDictionary?["CFBundleExecutable"] as? String else {
            return nil
        }
        guard let aClass: T.Type = NSClassFromString("\(nameSpace).\(className)") as? T.Type else {
            return nil
        }
        return aClass
    }
}

// MARK: - UI

extension UIView {
    func setWidthEqualSuperview() {
        guard superview != nil else { fatalError("Cannot working without superview") }
        leftAnchor.constraint(equalTo: superview!.leftAnchor).isActive = true
        rightAnchor.constraint(equalTo: superview!.rightAnchor).isActive = true
    }
}

extension AlertViewProtocol where Self: UIViewController {
    func presentAlertWith(title: String = "", message: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
}
